import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function getRandomIntInclusive(min, max) {
    // Standard Random Number Generator for JS
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

function dSix() {
    // Use the random num generator to return results for a standard D6 dice
    var num = getRandomIntInclusive(1,6);
    return num;
}

function Dice(props) {
    console.log(props.value);
    return (
        <div>
            <button onClick={props.onClick}>
                Roll Dice
            </button>
            <h1>Free One: {props.value[0]}</h1>
            <h1>Free Two: {props.value[1]}</h1>
            <h1>Red: {props.value[2]}</h1>
            <h1>Yellow: {props.value[3]}</h1>
            <h1>Green: {props.value[4]}</h1>
            <h1>Blue: {props.value[5]}</h1>
        </div>

    );
}
function rollArray(item, index, arr) {
    arr[index] = dSix();
}
class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dice: Array(6).fill(0) 
        }
    }
    handleClick() {
        const current = this.state.dice.slice();
        current.forEach(rollArray);
        this.setState({
            dice: current
        });
    }
    render() {
      return (
        <div className="game">
          <div className="game-sheet">
              <Dice
                value = {this.state.dice}
                onClick={() => this.handleClick()} 
              ></Dice>
          </div>
        </div>
      );
    }
  }
ReactDOM.render(<Game />, document.getElementById("root"));